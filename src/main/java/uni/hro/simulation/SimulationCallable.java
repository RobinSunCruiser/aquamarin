package uni.hro.simulation;

import com.anji.integration.Activator;
import com.anji.integration.ActivatorTranscriber;
import uni.hro.model.*;
import uni.hro.model.HitBoxInputValueGenerator;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Created by xor on 5/29/17.
 */
public class SimulationCallable implements Callable<List<WorldEntity>> {
    private final WorldEntity[] worldEntities;
    private final int startPos;
    private final int endPos;
    private final ActivatorTranscriber factory;
    private List<WorldEntity> probablyExistingEntities;

    public SimulationCallable(WorldEntity[] worldEntities, int startPos, int endPos, ActivatorTranscriber factory) {
        this.worldEntities = worldEntities;
        this.startPos = startPos;
        this.endPos = endPos;
        this.factory = factory;
        this.probablyExistingEntities = new ArrayList<>(endPos);
    }

    @Override
    public List<WorldEntity> call() throws Exception {
        for (int i = startPos; i < endPos; i++) {
            WorldEntity worldEntity = worldEntities[i];
            try {
                probablyExistingEntities.add(worldEntity);
                if (worldEntity instanceof Creature) {
                    Creature creature = (Creature) worldEntity;
                    if (creature.isAlive()) {
                        creature.perceive();
                        creature.handleCollisions();
                        creature.swim();
                        creature.age();
                        double[] inputs = creature.createInputArray();
                        int count = fillArray(inputs, 0, creature.getInputValueGenerators());
                         Activator activator = creature.getActivator(factory);//factory.newActivator(creature.getChromosome());
                        double[] actions = activator.next(inputs);
                        creature.moveN(actions[0]);
                        creature.accelerate(actions[1]);
                        creature.moveS(actions[2]);
                        creature.decelerate(actions[3]);
                        
                        
                      
                    } else {
                        creature.sink();
                    }
                } else if (worldEntity instanceof Food) {
                    if (!((Food) worldEntity).sink()) {
                        ((Consumable) worldEntity).consume();
                    }
                }
                if (worldEntity instanceof Plant) {
                    ((Plant) worldEntity).plantsInTheCurrentMovement();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return probablyExistingEntities;
    }

    private static <T> int fillArray(double[] arr, int pos, List<HitBoxInputValueGenerator> neurons) {
        for (HitBoxInputValueGenerator neuron : neurons)
            arr[pos++] = neuron.getValue();
        return pos;
    }

    private static int[] getHighestTwoValues(double[] in) {
        int[] result = new int[2];
        int first = 0;
        int sec = 0;
        double v1 = 0;
        double v2 = 0;
        for (int i = 0; i < in.length; i++) {
            double v = in[i];
            if (v > v1) {
                v1 = v;
                first = i;
            } else if (v > v2) {
                sec = i;
                v2 = v;
            }
        }
        result[0] = first;
        result[1] = sec;
        return result;
    }
}
