package uni.hro.simulation;

import com.anji.integration.ActivatorTranscriber;
import com.anji.neat.DummyEvolver;
import com.anji.neat.Evolver;
import com.anji.util.Properties;
import com.anji.util.Reset;
import org.jgap.Chromosome;
import uni.hro.model.*;
import uni.hro.server.Settings;

import java.io.InputStream;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by xor on 5/15/17.
 */
public class Simulation {
	private final int noOfKraken;
	private final int noOfFishes;
	private final int aquariumWidth;
	private final int aquariumHeight;
	private final int noOfCores;
	private ActivatorTranscriber factory;
	private World world;
	private Evolver fishEvolver, krakenEvolver;
	private ExecutorService executor;
	private int stepNumber;
	private Set<WorldEntity> allCreatures;
	private String fishProps = "/uni/hro/nn/fish.properties";
	private String krakenProps = "/uni/hro/nn/kraken.properties";
	private Hashtable<Creature, Void> deadStuff = new Hashtable<>();
	private Random random = new Random();

	// probability of food spawning
	private double generalfoodSpawnProb = 0.01;

	// adapted foodSpawnProb to actual screensize
	private double adaptedfoodSpawnProb;

	public Simulation(Integer numberOfFishes, Integer numberOfKraken, int numberOfPlants, int numberOfStones,
			int aquariumWidth, int aquariumHeight) {
		this.world = World.createRandomWorld1(numberOfFishes, numberOfKraken, numberOfPlants, numberOfStones,
				aquariumWidth, aquariumHeight);

		noOfFishes = numberOfFishes;
		noOfKraken = numberOfKraken;
		this.aquariumWidth = aquariumWidth;
		this.aquariumHeight = aquariumHeight;

		noOfCores = Runtime.getRuntime().availableProcessors();
		try {
			executor = Executors.newFixedThreadPool(noOfCores);
			Properties props = new Properties();
			InputStream in = getClass().getResourceAsStream(fishProps);
			props.load(in);
			in.close();
			props.setProperty("stimulus.size", Fish.calcInputSize().toString());
			props.setProperty("popul.size", numberOfFishes.toString());
			fishEvolver = numberOfFishes > 0 ? Evolver.instance(props) : new DummyEvolver();
			fishEvolver.setName("fishes");
			in = getClass().getResourceAsStream(krakenProps);
			props = new Properties();
			props.load(in);
			in.close();
			props.setProperty("stimulus.size", Kraken.calcInputSize().toString());
			props.setProperty("popul.size", numberOfKraken.toString());
			krakenEvolver = numberOfKraken > 0 ? Evolver.instance(props) : new DummyEvolver();
			krakenEvolver.setName("krakense");
			factory = (ActivatorTranscriber) props.singletonObjectProperty(ActivatorTranscriber.class);

		} catch (Throwable throwable) {
			throwable.printStackTrace();
		}
	}

	public void evolve() {
		fishEvolver.evolveGeneration();
		krakenEvolver.evolveGeneration();
	}

	/**
	 * simulates a whole generation
	 *
	 * @return
	 */
	public boolean simulate() {
		float xcoordinate, ycoordinate; // x- and y-Position of randomly
										// generated foodpallets

		int minScreenWidth = 30, maxScreenWidth = world.getWidth() - 30;// Region
																		// where
																		// the
																		// food
																		// may
																		// spawn
		int minScreenHeight = 30, maxScreenHeight = world.getHeight() - 30;

		for (int i = 0; i < 1; i++) {
			if (new Random().nextFloat() > (1 - adaptedfoodSpawnProb)) {
				xcoordinate = World.generateRandom(minScreenWidth, maxScreenWidth);
				ycoordinate = World.generateRandom(minScreenHeight, maxScreenHeight);
				world.getEntities().add(new Food(world, xcoordinate, ycoordinate));
			}
		}
		WorldEntity[] entities = world.getEntities().toArray(new WorldEntity[0]);
		return process(entities);
	}

	private boolean process(WorldEntity[] entities) {
		stepNumber++;
		boolean everythingIsDead = true;
		boolean OneOrZeroCreaturesAreAlive = false;
		List<WorldEntity> probablyExistingStuff = new ArrayList<>(entities.length);
		try {
			int noOfThreads = noOfCores;
			int size = entities.length;
			if (size < noOfThreads)
				noOfThreads = size;
			if (size < 1)
				return true;
			int width = size / noOfThreads;
			int rest = size - noOfThreads * width;
			int startIndex = 0;
			int endIndex = width;
			List<SimulationCallable> callables = new ArrayList<>();
			for (int i = 0; i < noOfThreads; i++) {
				if (rest > 0) {
					endIndex++;
					rest--;
				}
				SimulationCallable callable = new SimulationCallable(entities, startIndex, endIndex, factory);
				callables.add(callable);
				startIndex = endIndex;
				endIndex += width;
			}
			List<Future<List<WorldEntity>>> futures = executor.invokeAll(callables);
			for (Future<List<WorldEntity>> future : futures) {
				List<WorldEntity> futureResult = future.get();
				if (futureResult != null) {
					probablyExistingStuff.addAll(futureResult);
				}
			}
			// remove consumed WorldEntities
			world.getEntities().clear();
			Collection<WorldEntity> existingStuff = world.getEntities();

			int aliveCreatureCounter = 0;
			for (WorldEntity worldEntity : probablyExistingStuff) {
				if (worldEntity instanceof Creature) {
					Creature creature = (Creature) worldEntity;
					if (creature.isAlive()) {

						aliveCreatureCounter++;

						// Exchanged for another condition
						// everythingIsDead = false;

					}

				}
				if (worldEntity instanceof Consumable) {
					Consumable consumable = (Consumable) worldEntity;
					if (!consumable.isConsumed())
						existingStuff.add(consumable);
				} else {
					existingStuff.add(worldEntity);
				}

			}

			if (aliveCreatureCounter <= 1) {

				

				for (WorldEntity worldEntity : probablyExistingStuff) {
					if (worldEntity instanceof Fish) {
						Fish fish = (Fish) worldEntity;
						if (fish.isAlive()) {

							System.out.println("am Leben 1");
							fish.consume();
							OneOrZeroCreaturesAreAlive = true;
						}
					
					}
					
					
				}
				
				if (aliveCreatureCounter == 0) {
					OneOrZeroCreaturesAreAlive = true;
				}
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return OneOrZeroCreaturesAreAlive;
	}

	public World getWorld() {
		return world;
	}

	public void prepare(boolean removeFood) {
		stepNumber = 0;

		List<Chromosome> fishChroms = fishEvolver.nextGeneration();
		List<Chromosome> krakenChroms = krakenEvolver.nextGeneration();
		int fishCount = 0;
		int krakenCount = 0;

		Set<WorldEntity> entities = new HashSet<>(world.getEntities());
		world.getEntities().clear();

		adaptedfoodSpawnProb = calculateFoodAmount(generalfoodSpawnProb);

		for (WorldEntity worldEntity : entities) {
			if (worldEntity instanceof Food) {
				if (!removeFood)
					world.getEntities().add(worldEntity);
			} else if ((worldEntity instanceof Creature)) {

			} else {
				world.getEntities().add(worldEntity);
			}
		}
		for (int i = 0; i < noOfFishes; i++) {
			Fish fish = new Fish(world, random(aquariumWidth), random(aquariumHeight));
			fish.setChromosome(fishChroms.get(fishCount++));
			world.getEntities().add(fish);
		}
		for (int i = 0; i < noOfKraken; i++) {
			Kraken kraken = new Kraken(world, random(aquariumWidth), random(aquariumHeight));
			kraken.setChromosome(krakenChroms.get(krakenCount++));
			world.getEntities().add(kraken);
		}
		for (int i = 0; i < 50; i++) {
			Food food = new Food(world, random(aquariumWidth), random(aquariumHeight));
			world.getEntities().add(food);
		}
	}

	private float random(float range) {
		return random.nextFloat() * range;
	}

	public void reset() {
		try {
			System.out.print("Deleting previous creatures!");
			// Reset fish
			Properties props = new Properties();
			InputStream in = getClass().getResourceAsStream(fishProps);
			props.load(in);
			in.close();
			Reset resetter = new Reset(props);
			resetter.setUserInteraction(false);
			resetter.reset();
			// Reset kraken
			props = new Properties();
			in = getClass().getResourceAsStream(krakenProps);
			props.load(in);
			in.close();
			resetter = new Reset(props);
			resetter.setUserInteraction(false);
			resetter.reset();

			Settings lastRunSettings;
			if (Settings.exists())
				lastRunSettings = Settings.load();
			else
				lastRunSettings = new Settings(0, 15, 2, 1, 1, 480, 360);
			lastRunSettings.setNumGen(1);
			lastRunSettings.store();
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}

	/**
	 * Calculates an appropriate probability to spawn food for the current
	 * world.
	 * 
	 * @param foodSpawnProb
	 * @return
	 */
	public double calculateFoodAmount(double foodSpawnProb) {

		double fullHdDiagonalLength = Math.sqrt(1920 * 1920 + 1080 * 1080);

		double worldDiagonalLenght = Math
				.sqrt(this.aquariumWidth * this.aquariumWidth + this.aquariumHeight * this.aquariumHeight);

		double worldSizefactor = fullHdDiagonalLength / worldDiagonalLenght;

		return foodSpawnProb / worldSizefactor;

	}
}
