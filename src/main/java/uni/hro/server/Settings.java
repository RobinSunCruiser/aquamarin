package uni.hro.server;

import java.io.*;

public class Settings implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final String path = "\\db\\settings.ser";
	private int num_gen, num_fish, num_kraken, num_plant, num_stone, view_x, view_y;
	
	public Settings(int gen, int fish, int kraken, int plant, int stone, int x, int y) {
		num_gen = gen;
		num_fish = fish;
		num_kraken = kraken;
		num_plant = plant;
		num_stone = stone;
		view_x = x;
		view_y = y;
	}
	
	public Settings() {
		num_gen = 0;
		num_fish = 15; 
		num_kraken = 2;
		num_plant = 1; 
		num_stone = 1; 
		view_x = 480;
		view_y = 360;
	}
	
	public Settings(Settings set) {
		num_gen = set.getNumGen();
		num_fish = set.getNumFish();
		num_kraken = set.getNumKraken();
		num_plant = set.getNumPlant();
		num_stone = set.getNumStone();
		view_x = set.getX();
		view_y = set.getY();
	}
	
	/**
	 * Loading in the settings from the last run.
	 * 
	 * @return
	 * @throws IOException
	 */
	public static Settings load() {
		Settings settings;
		try {
			File dest = new File(System.getProperty("user.dir") + path);
			FileInputStream fos = new FileInputStream(dest);
			ObjectInputStream oos = new ObjectInputStream(fos);
			settings = (Settings) oos.readObject();
			oos.close();
			fos.close();
		} catch (IOException e) {
			return new Settings();
		} catch (ClassNotFoundException e) {
			return new Settings();
		}
		return settings;
	}
	
	/**
	 * Stores the settings to the standard location.
	 */
	public void store() {
		try {
			File dest = new File(System.getProperty("user.dir") + path);
			FileOutputStream fos = new FileOutputStream(dest);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(this);
			oos.close();
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Checks if a settings file exists in the standard directory.
	 * 
	 * @return
	 */
	public static boolean exists() {
		File f = new File(System.getProperty("user.dir") + path);
		return f.exists();
	}
	
	/**
	 * Deletes the settings file located in the standard directory.
	 * 
	 * @return
	 */
	public static boolean delete() {
		File f = new File(System.getProperty("user.dir") + path);
		return f.delete();
	}
	
	/**
	 * Returns true, if the number of fishes and kraken is equal in both settings.
	 * 
	 * @param settings
	 * @return
	 */
	public boolean match(Settings settings) {
		return softMatch(settings);
	}
	
	/**
	 * Returns true, if the number of fishes and kraken is equal in both settings.
	 * 
	 * @param s1
	 * @param s2
	 * @return
	 */
	public static boolean match(Settings s1, Settings s2) {
		return s1.match(s2);
	}
	
	/**
	 * Checks if the settings in are equal according to the mode. The following modes are available:
	 * "soft":	Settings are equal, if the number of fishes and kraken is equal.
	 * "hard":	Settings are equal, if the number of fishes, kraken, stones, plants and the display settings are equal.
	 * 
	 * @param settings
	 * @param mode
	 * @return
	 */
	public boolean match(Settings settings, String mode) {
		switch (mode) {
			case "soft": 	return softMatch(settings);
			case "hard": 	return hardMatch(settings);
			default: 		return false;
		}
	}
	
	/**
	 * Checks if the settings in are equal according to the mode. The following modes are available:
	 * "soft":	Settings are equal, if the number of fishes and kraken is equal.
	 * "hard":	Settings are equal, if the number of fishes, kraken, stones, plants and the display settings are equal.
	 * 
	 * @param s1
	 * @param s2
	 * @param mode
	 * @return
	 */
	public static boolean match(Settings s1, Settings s2, String mode) {
		return s1.match(s2, mode);
	}

	/**
	 * Checks only for equality in number of fishes and kraken.
	 * 
	 * @param settings
	 * @return
	 */
	private boolean softMatch(Settings settings) {
		if (num_fish == settings.getNumFish()
				&& num_kraken == settings.getNumKraken())
			return true;
		else return false;
	}
	
	/**
	 * Checks for equality in number of fishes, kraken, stones and plants as well as the display settings.
	 * 
	 * @param settings
	 * @return
	 */
	private boolean hardMatch(Settings settings) {
		if (num_plant == settings.getNumPlant()
				&& num_stone == settings.getNumStone()
				&& view_x == settings.getX()
				&& view_y == settings.getY())
			return softMatch(settings);
		else return false;
	}
	
	public int getNumGen() {
		return num_gen;
	}
	
	public void setNumGen(int gen) {
		num_gen = gen;
	}
	
	public int getNumFish() {
		return num_fish;
	}

	public void setNumFish(int fish) {
		num_fish = fish;
	}
	
	public int getNumKraken() {
		return num_kraken;
	}
	
	public void setNumKraken(int kraken) {
		num_kraken = kraken;
	}
	
	public int getNumPlant() {
		return num_plant;
	}
	
	public void setNumPlant(int plant) {
		num_plant = plant;
	}
	
	public int getNumStone() {
		return num_stone;
	}
	
	public void setNumStone(int stone) {
		num_stone = stone;
	}
	
	public int getX() {
		return view_x;
	}
	
	public void setX(int x) {
		view_x = x;
	}
	
	public int getY() {
		return view_y;
	}
	
	public void setY(int y) {
		view_y = y;
	}
	
}
