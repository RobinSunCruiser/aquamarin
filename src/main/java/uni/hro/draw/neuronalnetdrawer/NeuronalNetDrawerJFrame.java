package uni.hro.draw.neuronalnetdrawer;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.jgap.Chromosome;

import java.awt.Dimension;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;

/**
 * @author Columbus
 *
 */
public class NeuronalNetDrawerJFrame extends JFrame {

	private static NeuronalNetDrawerJFrame main = new NeuronalNetDrawerJFrame();
	
	public void drawChromosome(Chromosome chromosome) {

		GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
		int ScreenWidth = gd.getDisplayMode().getWidth();
		int ScreenHeight = gd.getDisplayMode().getHeight();
		
		NeuronalNetDrawerJPanel neuronalNetDrawerJPanel = new NeuronalNetDrawerJPanel(chromosome);

		JScrollPane jScrollPane = new JScrollPane(neuronalNetDrawerJPanel);
		jScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

	
		jScrollPane.setBounds(0, 0, ScreenWidth-jScrollPane.getVerticalScrollBar().getWidth()-50-jScrollPane.getVerticalScrollBar().getWidth(), ScreenHeight-50-jScrollPane.getVerticalScrollBar().getWidth());
		JPanel contentPane = new JPanel(null);
		contentPane.setPreferredSize(new Dimension(ScreenWidth-50, ScreenHeight-50));
		contentPane.add(jScrollPane);

		neuronalNetDrawerJPanel.setMinimumSize(new Dimension(20000, 20000));
		neuronalNetDrawerJPanel.setPreferredSize(new Dimension(20000, 20000));

		
		main.setContentPane(contentPane);
		main.pack();
		main.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		main.setVisible(true);
	}
}
