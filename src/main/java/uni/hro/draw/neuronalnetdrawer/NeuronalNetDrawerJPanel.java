package uni.hro.draw.neuronalnetdrawer;

import javax.swing.*;

import org.jgap.Chromosome;

import com.anji.neat.ConnectionAllele;
import com.anji.neat.NeuronAllele;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.SortedSet;

/**
 * @author
 */
public class NeuronalNetDrawerJPanel extends JPanel {

	Chromosome chromosome;
	protected boolean mousePressed = false;
	private SortedSet alleles;

	ArrayList<ArrayList<Neuron>> neurons = new ArrayList<ArrayList<Neuron>>();
	ArrayList<Neuron> inputneurons = new ArrayList<Neuron>();
	ArrayList<Neuron> outputneurons = new ArrayList<Neuron>();
	ArrayList<Neuron> hiddenneurons = new ArrayList<Neuron>();
	ArrayList<Connection> connections = new ArrayList<Connection>();

	Color highlightColor = new Color(63, 72, 204);
	Color inColor = new Color(63, 204, 72);
	Color outColor = new Color(204, 72, 63);
	Color lowightColor = new Color(200, 200, 200);

	/**
	 * @param main
	 */
	public NeuronalNetDrawerJPanel(Chromosome chromosome) {

		this.chromosome = chromosome;

		// drawworld = this;
		this.alleles = chromosome.getAlleles();

		this.addMouseListener(new MouseAdapter() {

			@Override

			public void mouseClicked(MouseEvent e) {

				if (e.getButton() == MouseEvent.BUTTON1) {

					int xOfMouseClick = e.getX();
					int yOfMouseClick = e.getY();

					// System.out.println(xOfMouseClick + " " + yOfMouseClick);
					highlightTheClickedObject(xOfMouseClick, yOfMouseClick);

					repaint();
				}

			}

		});

		prepareDrawing();

		this.repaint();
	}

	public void prepareDrawing() {

		int inputneuronCounter = 0;
		int outputneuronCounter = 1;
		int hiddenneuronCounter = 0;
		int distanceBetweenNeurons = 40;
		long id;

		for (Object allele : alleles) {

			if (allele instanceof NeuronAllele) {

				if (inputneuronCounter < 152) {

					id = ((NeuronAllele) allele).getInnovationId();
					Inputneuron inputneuron = new Inputneuron(id, 100,
							100 + inputneuronCounter * distanceBetweenNeurons);
					inputneurons.add(inputneuron);
					inputneuronCounter++;
				}

				else {

					if (outputneuronCounter < 5) {
						id = ((NeuronAllele) allele).getInnovationId();

						Outputneuron outputneuron = new Outputneuron(id, 900,
								400 + outputneuronCounter * distanceBetweenNeurons);
						outputneurons.add(outputneuron);
						outputneuronCounter++;
					}

					else {

						id = ((NeuronAllele) allele).getInnovationId();

						Hiddenneuron hiddenneuron = new Hiddenneuron(id, 450,
								100 + hiddenneuronCounter * distanceBetweenNeurons);
						hiddenneurons.add(hiddenneuron);
						hiddenneuronCounter++;
					}
				}
			}

			if (allele instanceof ConnectionAllele) {

				ConnectionAllele connectionAllele = ((ConnectionAllele) allele);
				id = connectionAllele.getInnovationId();

				long sourceNeuronId = connectionAllele.getSrcNeuronId();
				long destinationNeuronId = connectionAllele.getDestNeuronId();
				double weight = connectionAllele.getWeight();

				Connection connection = new Connection(id, sourceNeuronId, destinationNeuronId, weight);
				connections.add(connection);
			}

		}

		connectionsfindNeurons();

		checkBestDrawingPosition();

		neurons.add(inputneurons);
		neurons.add(outputneurons);
		neurons.add(hiddenneurons);
	}

	/**
	 * 
	 */
	private void checkBestDrawingPosition() {

		int highestHiddenneuronCenterX = 700;
		int distanceBetweenNeurons = 100;
		for (Neuron neuron : hiddenneurons) {

			for (Connection con : neuron.getConnList()) {

				/**
				 * Wenn es ein Endneuron ist
				 */
				if (neuron.getId() == con.getEndneuronId()) {

					int xOfStartneuron = con.getStartNeuron().getCenterX();
					int xOfEndneuron = con.getEndNeuron().getCenterX();
					if ((xOfEndneuron - xOfStartneuron) < distanceBetweenNeurons) {

						neuron.setCenterX(xOfEndneuron + distanceBetweenNeurons);
						neuron.setX(xOfEndneuron + distanceBetweenNeurons - neuron.getSize() / 2);

						if (highestHiddenneuronCenterX < (xOfEndneuron + distanceBetweenNeurons)) {
							highestHiddenneuronCenterX = (xOfEndneuron + distanceBetweenNeurons);
						}
					}

				}
			}

		}

		for (Neuron neuron : outputneurons) {

			neuron.setCenterX(highestHiddenneuronCenterX + 200);

			neuron.setX(highestHiddenneuronCenterX + 200 - neuron.getSize() / 2);

			for (Connection con : neuron.getConnList()) {

				if (neuron.getId() == con.getEndneuronId()) {

					int xOfStartneuron = con.getStartNeuron().getCenterX();
					int xOfEndneuron = con.getEndNeuron().getCenterX();
					if ((xOfEndneuron - xOfStartneuron) < distanceBetweenNeurons) {

						neuron.setCenterX(xOfEndneuron + distanceBetweenNeurons);

						neuron.setX(xOfEndneuron + distanceBetweenNeurons - neuron.getSize() / 2);
					}

				}
			}

		}

	}

	/**
	 * 
	 * @param xOfMouseClick
	 * @param yOfMouseClick
	 */
	private void highlightTheClickedObject(int xOfMouseClick, int yOfMouseClick) {

		int objectSize = 20;
		int counter = 0;

		boolean nothingWasClicked = true;

		for (ArrayList<Neuron> neuronlist : neurons) {
			for (Neuron neuron : neuronlist) {
				neuron.setColor(lowightColor);
				neuron.setHighlighted(false);
				if ((neuron.getCenterX() - objectSize / 2) <= xOfMouseClick
						&& (neuron.getCenterX() + objectSize / 2) >= xOfMouseClick) {

					// System.out.println("true for " + neuron.getCenterX());
					if ((neuron.getCenterY() - objectSize / 2) <= yOfMouseClick
							&& (neuron.getCenterY() + objectSize / 2) >= yOfMouseClick) {

						// System.out.println("true for " +
						// neuron.getCenterY());
						neuron.setColor(highlightColor);
						nothingWasClicked = false;
						neuron.setHighlighted(true);

						highlightAllConnectedElements(neuron);
					}

				}

				counter++;
			}

		}

		for (ArrayList<Neuron> neuronlist : neurons) {
			for (Neuron neuron : neuronlist) {

				if (nothingWasClicked == true) {
					neuron.setColor(Color.black);

					for (Connection con : neuron.getConnList()) {
						con.setHighlighted(false);
					}
				}
			}

			for (Connection con : connections) {

				if (nothingWasClicked == true) {
					con.setColor(Color.black);
				}
			}
		}
	}

	/**
	 * @param neuron
	 */
	private void highlightAllConnectedElements(Neuron neuron) {
		// TODO:

		for (Connection con : connections) {
			con.setColor(lowightColor);
			con.setHighlighted(false);
		}

		for (Connection con : neuron.getConnList()) {

			if (neuron == con.getEndNeuron()) {
				con.setColor(inColor);
				con.setHighlighted(true);
			}

			else {
				con.setColor(outColor);
				con.setHighlighted(true);
			}
		}

	}

	/**
	 * @param connection
	 */
	private void connectionsfindNeurons() {

		int counter = 0;

		for (Connection connection : connections) {

			long startneuronId = connection.getStartneuronId();
			long endneuronId = connection.getEndneuronId();

			int neuroncounter = 0;
			for (Neuron neuron : inputneurons) {

				if (startneuronId == neuron.getId()) {

					Connection con = connections.get(counter);
					con.setStartNeuron(neuron);

					connections.set(counter, con);

					neuron.addToConnList(connection);

				}
				neuroncounter++;
			}

			for (Neuron neuron : hiddenneurons) {

				if (startneuronId == neuron.getId()) {

					Connection con = connections.get(counter);
					con.setStartNeuron(neuron);

					connections.set(counter, con);

					neuron.addToConnList(connection);
				}
			}

			for (Neuron neuron : hiddenneurons) {

				if (endneuronId == neuron.getId()) {

					Connection con = connections.get(counter);
					con.setEndNeuron(neuron);

					connections.set(counter, con);

					neuron.addToConnList(connection);
				}
			}

			for (Neuron neuron : outputneurons) {
				if (endneuronId == neuron.getId()) {

					Connection con = connections.get(counter);
					con.setEndNeuron(neuron);

					connections.set(counter, con);

					neuron.addToConnList(connection);
				}

			}

			for (Neuron neuron : outputneurons) {
				if (startneuronId == neuron.getId()) {

					Connection con = connections.get(counter);
					con.setStartNeuron(neuron);

					connections.set(counter, con);

					neuron.addToConnList(connection);
				}

			}

			counter++;

		}

	}

	/**
	 * Gibt an, was auf das AquariumJPanel gezeichnet werden soll.
	 */
	public void paintComponent(Graphics g) {

		super.paintComponent(g);

		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		g2.drawString("Hint: Click on a neuron to highlight it and get weight information", 22, 22);

		for (Connection connection : connections) {

			String id = Long.toString(connection.getId());
			String weight = Double.toString(connection.getWeight());

			String formatedWeight = new DecimalFormat("##.##").format(connection.getWeight());

			int startX = connection.getStartNeuron().getCenterX();
			int startY = connection.getStartNeuron().getCenterY();
			int endX = connection.getEndNeuron().getCenterX();
			int endY = connection.getEndNeuron().getCenterY();

			int centerX = (endX - startX) / 2;
			int centerY = (endY - startY) / 2;
			Color color = connection.getColor();

			if (startX < endX) {
				centerX = startX + (endX - startX) / 2;
			} else {
				centerX = endX + (startX - endX) / 2;
			}

			if (startY < endY) {
				centerY = startY + (endY - startY) / 2;
			} else {
				centerY = endY + (startY - endY) / 2;
			}

			g2.setColor(color);
			g2.setStroke(new BasicStroke(1));
			g2.drawLine(startX, startY, endX, endY);

		
		}

		drawHighlightedConnections(g2);

		for (Neuron neuron : inputneurons) {

			String id = Long.toString(neuron.getId());
			int x = neuron.getX();
			int y = neuron.getY();
			int size = neuron.getSize();
			Color color = neuron.getColor();

			g2.setColor(Color.white);
			g2.fillRect(x, y, size, size);
			g2.setColor(color);
			g2.drawRect(x, y, size, size);
			
			if(neuron.isHighlighted()==true)
			{
				g2.setFont(new Font("default", Font.BOLD, 12));
				g2.drawString(id, x + 2, y + 13);
				g2.setFont(new Font("default", Font.PLAIN, 12));
			}
			else
			{
				g2.drawString(id, x + 2, y + 13);
			}
		}

		for (Neuron neuron : hiddenneurons) {

			String id = Long.toString(neuron.getId());
			int x = neuron.getX();
			int y = neuron.getY();
			Color color = neuron.getColor();
			int size = neuron.getSize();
			g2.setColor(Color.white);
			g2.fillOval(x, y, size, size);
			g2.setColor(color);
			g2.drawOval(x, y, size, size);
			
			
			if(neuron.isHighlighted()==true)
			{
				g2.setFont(new Font("default", Font.BOLD, 12));
				g2.drawString(id, x, y + 32);
				g2.setFont(new Font("default", Font.PLAIN, 12));
			}
			else
			{
				g2.drawString(id, x, y + 32);
			}
		}

		for (Neuron neuron : outputneurons) {
			
			String id = Long.toString(neuron.getId());

			int x = neuron.getX();
			int y = neuron.getY();
			Color color = neuron.getColor();
			int size = neuron.getSize();
			int[] xPoints = { x, x + size / 2, x + size };
			int[] yPoints = { y + size, y + 0, y + size };
			int nPoints = 3;
			g2.setColor(Color.white);
			g2.fillPolygon(xPoints, yPoints, nPoints);
			g2.setColor(color);
			g2.drawPolygon(xPoints, yPoints, nPoints);
			
			
			String outputAction=""; 
			if ("152".equals(id )) {
				outputAction= "Move North";
			}
			else if ("153".equals(id )) {
				outputAction= "Accelerate";
			}
			else if ("154".equals(id )) {
				 outputAction= "Move South";
			}
			else if ("155".equals(id )) {
				outputAction= "Decelerate";
			}
			
			
			if(neuron.isHighlighted()==true)
			{
				g2.setFont(new Font("default", Font.BOLD, 12));
				g2.drawString(id + ": " + outputAction, x, y + 32);
				
			
				
				
				g2.setFont(new Font("default", Font.PLAIN, 12));
			}
			else
			{
				g2.drawString(id + ": " + outputAction, x, y + 32);
			}
		}

	}

	/**
	 * 
	 */
	private void drawHighlightedConnections(Graphics2D g2) {
		
	
		
		for (Connection connection : connections) {
			if (connection.isHighlighted() == true) {

				
				
				Color color = connection.getColor();
				int startX = connection.getStartNeuron().getCenterX();
				int startY = connection.getStartNeuron().getCenterY();
				int endX = connection.getEndNeuron().getCenterX();
				int endY = connection.getEndNeuron().getCenterY();
				int centerX = (endX - startX) / 2;
				int centerY = (endY - startY) / 2;
				
				g2.setColor(color);
				g2.setStroke(new BasicStroke(1));
				g2.drawLine(startX, startY, endX, endY);
				
				
				
			}
			
			
		}
		
		for (Connection connection : connections) {

			if (connection.isHighlighted() == true) {

				int startX = connection.getStartNeuron().getCenterX();
				int startY = connection.getStartNeuron().getCenterY();
				int endX = connection.getEndNeuron().getCenterX();
				int endY = connection.getEndNeuron().getCenterY();
				int centerX = (endX - startX) / 2;
				int centerY = (endY - startY) / 2;
				if (startX < endX) {
					centerX = startX + (endX - startX) / 2;
				} else {
					centerX = endX + (startX - endX) / 2;
				}

				if (startY < endY) {
					centerY = startY + (endY - startY) / 2;
				} else {
					centerY = endY + (startY - endY) / 2;
				}
				
				Color color = connection.getColor();
				String formatedWeight = new DecimalFormat("##.##").format(connection.getWeight());
				g2.setColor(color.darker());
				g2.setFont(new Font("default", Font.BOLD, 12));
				g2.drawString("w: " + formatedWeight, centerX, centerY - 11);
				g2.setFont(new Font("default", Font.PLAIN, 12));
			}

		}
	}
}
