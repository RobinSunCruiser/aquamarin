/**
 * 
 */
package uni.hro.draw.neuronalnetdrawer;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * @author Columbus
 *
 */
public class CodeSchnipsel {

	
	public static void main(String args[])
	{
	
		
		  try {
			  
			  /**
				 *  Auflisten, welche XML-Dateien sich im Pfad befinden
				 */
			  List<File> filesInFolder = Files.walk(Paths.get("nevt/fish/fitness"))
                      .filter(Files::isRegularFile)
                      .map(Path::toFile)
                      .collect(Collectors.toList());
			  
			  
			  
			  for(File filepath:  filesInFolder)
			  {
			  System.out.println(filesInFolder);
				File fXmlFile = new File(filepath.toString());
				
				
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				
				dbFactory.setValidating(false);
				dbFactory.setNamespaceAware(true);
				dbFactory.setFeature("http://xml.org/sax/features/namespaces", false);
				dbFactory.setFeature("http://xml.org/sax/features/validation", false);
				dbFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
				dbFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
				
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				Document doc = dBuilder.parse(fXmlFile);
				doc.getDocumentElement().normalize();

				System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

				NodeList nList = doc.getElementsByTagName("generation");

				System.out.println("----------------------------");

				
				/**
				 * Werte der letzten Generation auslesen.
				 */
				
			
				
				Element element = (Element) nList.item(nList.getLength()-1);
				NodeList fitnessNodes = nList.item(nList.getLength()-1).getFirstChild().getChildNodes();
				
				nList.item(nList.getLength()-1).getNextSibling();
				for (int i = 0; i < fitnessNodes.getLength(); i++) {
					
					Node nNode = nList.item(i);
					
					Element element1 = (Element) nNode;
					System.out.println("Value : " + element1.getNodeValue());
				}
				System.out.println("neuron id : " + nList.item(nList.getLength()-1).getFirstChild());
				
				
				
				Element eElement = (Element) nList.item(nList.getLength()-1);
				
				
				System.out.println("neuron id : " + eElement.getElementsByTagName("max").item(0).getTextContent());
				System.out.println("neuron id : " + eElement.getElementsByTagName("min").item(0).getTextContent());
				System.out.println("neuron id : " + eElement.getElementsByTagName("avg").item(0).getTextContent());
				;
				/**
				for (int temp = 0; temp < nList.getLength(); temp++) {

					Node nNode = nList.item(temp);

					System.out.println("\nCurrent Element :" + nNode.getNodeName());

					if (nNode.getNodeType() == Node.ELEMENT_NODE) {

						Element eElement = (Element) nNode;

						System.out.println("neuron id : " + eElement.getAttribute("id"));
				//		System.out.println("First Name : " + eElement.getElementsByTagName("node").item(0).getTextContent());
				//		System.out.println("Last Name : " + eElement.getElementsByTagName("lastname").item(0).getTextContent());
				//		System.out.println("Nick Name : " + eElement.getElementsByTagName("nickname").item(0).getTextContent());
				//		System.out.println("Salary : " + eElement.getElementsByTagName("salary").item(0).getTextContent());

						
					  }
					}
				*/}
				
			    } catch (Exception e) {
				e.printStackTrace();
			    }
		
	//	main.setVisible(true);
	//	main.add(drawWorld);
	//	main.setSize(1000,1000);
		
		
		

	}

	
	
}
