/**
 * 
 */
package uni.hro.draw.neuronalnetdrawer;

import java.awt.Color;

/**
 * @author Columbus
 *
 */
public class Connection {

	private long id;
	private long startneuronId;
	private long endneuronId;
	private Neuron startNeuron;
	private Neuron endNeuron;
	private double weight;
	private Color color = Color.BLACK;
	private boolean highlighted =false;
	
	public Connection(long id, long startneuronId, long endneuronId, double weight) {
		this.startneuronId = startneuronId;
		this.endneuronId = endneuronId;
		this.weight = weight;

	}

	/**
	 * Setter und Getter
	 */

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getStartneuronId() {
		return startneuronId;
	}

	public void setStartneuronId(long startneuronId) {
		this.startneuronId = startneuronId;
	}

	public long getEndneuronId() {
		return endneuronId;
	}

	public void setEndneuronId(long endneuronId) {
		this.endneuronId = endneuronId;
	}

	public Neuron getStartNeuron() {
		return startNeuron;
	}

	public void setStartNeuron(Neuron startNeuron) {
		this.startNeuron = startNeuron;
	}

	public Neuron getEndNeuron() {
		return endNeuron;
	}

	public void setEndNeuron(Neuron endNeuron) {
		this.endNeuron = endNeuron;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public boolean isHighlighted() {
		return highlighted;
	}

	public void setHighlighted(boolean highlighted) {
		this.highlighted = highlighted;
	}
}
