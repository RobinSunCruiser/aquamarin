package uni.hro.draw;

/**
 * 
 */

import java.awt.*;
import java.awt.geom.*;

/**
 * @author Joerg
 *
 *         Diese Klasse bestimmt, wie das Vorspulsymbol gezeichnet wird.
 */

public class FastForwardSymbolDrawer {

	/**
	 * Diese Methode bestimmt, wie das Vorspulsymbol gezeichnet wird.
	 * 
	 * @param displayHeight
	 * @param displayWidth
	 * 
	 * @param graphics
	 *            Grafik des AquariumJPanel
	 * 
	 */
	public static void draw(Graphics g, int displayWidth, int displayHeight) {

		Graphics2D g2 = (Graphics2D) g;

		/**
		 * legt die Zeichengroesse des Symbols fest
		 */
		int size = 50;
		int positionX = displayWidth / 2 - size;
		int positionY = displayHeight / 2 - (size);

		/**
		 * SizefactorX / -Y bestimmen die Zeichengroesse des Fisches. Ueber den
		 */
		double sizefactorX = 1;
		double sizefactorY = 1;

		Color color = Color.WHITE;

		/**
		 * Verschiebung des Objekts
		 */
		g2.translate(positionX, positionY);

		/**
		 * Usprungszustand des Objekts
		 */
		AffineTransform atOriginal = g2.getTransform();

		/**
		 * Skalierung des Objekts
		 */
		AffineTransform at = new AffineTransform();
		at.setToScale(sizefactorX, sizefactorY);
		if (sizefactorX < 0) {
			g2.translate(-100 * sizefactorX, 0);
		}
		/**
		 * Transformationen anwenden
		 */
		g2.transform(at);

		/**
		 * Antialiasing aktiviert
		 */
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		/**
		 * Symbol malen
		 */

		int[] xPoints1 = { 0, size, 0 };
		int[] yPoints1 = { 0, size / 2, size, };
		int nPoints1 = 3;

		int[] xPoints2 = { size, size + size, size };
		int[] yPoints2 = { 0, size / 2, size, };
		int nPoints2 = 3;

		g2.setStroke(new BasicStroke(2));
		g2.setColor(color);

		g2.fillPolygon(xPoints1, yPoints1, nPoints1);
	//	g2.setColor(Color.BLACK);
	//	g2.drawPolygon(xPoints1, yPoints1, nPoints1);

		g2.setColor(color);
		g2.fillPolygon(xPoints2, yPoints2, nPoints2);
	//	g2.setColor(Color.BLACK);
	//	g2.drawPolygon(xPoints2, yPoints2, nPoints2);
		/**
		 * Transformation zuruecksetzen
		 */
		g2.setTransform(atOriginal);

		/**
		 * Reset Verschiebung des Objekts
		 */
		g2.translate(-positionX, -positionY);

	}

}