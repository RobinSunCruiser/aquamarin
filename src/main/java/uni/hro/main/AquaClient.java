package uni.hro.main;

import de.mein.auth.tools.RWSemaphore;
import uni.hro.networking.service.AquaClientService;

import java.io.File;

/**
 * Created by xor on 5/13/17.
 */
public class AquaClient extends Aqua<AquaClientService> {
    public static final File DEFAULT_CLIENT_DIRECTORY = new File("aqua.client.dir");

    public AquaClient(File directory, String name, int port, int portCert) {
        super(directory, name, port, portCert);
    }


    public static void main(String[] args) throws Exception {
        AquaClient aquaClient = new AquaClient(DEFAULT_CLIENT_DIRECTORY, "fish.client", 8890, 8891);
        aquaClient.startAqua();
        // die nächsten Zeilen verbinden mit dem Server (aber ohne Beachtung des GUI)
        //MeinAuthService meinAuthService = aquaClient.startAqua();
        //AquaClientService clientService = (AquaClientService) meinAuthService.getMeinServices().iterator().next();
        //Promise<MeinValidationProcess, Exception, Void> registered = meinAuthService.connect(null, "localhost", 8888, 8889, true);
        //registered.done(result -> N.r(() -> clientService.regAsClient("localhost", 8888, 8889)));
        // wait forever!
        RWSemaphore semaphore = new RWSemaphore();
        semaphore.lockWrite();
        semaphore.lockWrite();
    }

    @Override
    boolean isServer() {
        return false;
    }
}
