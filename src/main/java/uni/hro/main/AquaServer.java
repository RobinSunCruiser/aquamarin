package uni.hro.main;

import de.mein.auth.tools.RWSemaphore;
import uni.hro.networking.service.AquaServerService;

import java.io.File;

/**
 * Created by xor on 5/12/17.
 */
public class AquaServer extends Aqua<AquaServerService> {

    public static final File DEFAULT_SERVER_DIRECTORY = new File("aqua.server.dir");

    public AquaServer(File directory, String name, int port, int portCert) {
        super(directory, name, port, portCert);
    }


    public static void main(String[] args) throws Exception {
        AquaServer aquaServer = new AquaServer(DEFAULT_SERVER_DIRECTORY, "fish.server", 8888, 8889);
        aquaServer.startAqua();
        // wait forever!
        RWSemaphore semaphore = new RWSemaphore();
        semaphore.lockWrite();
        semaphore.lockWrite();
    }


    @Override
    boolean isServer() {
        return true;
    }
}
