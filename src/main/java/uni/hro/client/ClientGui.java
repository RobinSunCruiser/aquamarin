package uni.hro.client;

import de.mein.auth.tools.N;
import uni.hro.networking.service.AquaClientService;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * @author Johann
 *         GUI of the client application (should only be the view/aquarium).
 */
public class ClientGui extends JFrame {

    private static GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
    private final AquaClientService clientSevice;
    private JPanel contentPane;
    private JTextField serverAddressField;
    private JButton button1;
    private JButton button2;
    private JButton button3;
    private JButton button4;
    private JButton button5;
    private JButton button6;
    private JButton button7;
    private JButton button8;
    private JButton button9;
    private JButton button0;
    private JButton buttonDot;
    private JButton buttonDel;
    private JButton connectButton;
    private JButton exitButton;
    private JButton connectLButton;

    /**
     * Create the frame.
     */
    public ClientGui(AquaClientService clientService) {
        this.clientSevice = clientService;
        setMinimumSize(new Dimension(319, 479));
        setTitle("Aquarium Client");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 450, 228);

        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new BorderLayout(0, 0));

        serverAddressField = new JTextField();
        serverAddressField.setFont(new Font("Tahoma", Font.PLAIN, 18));
        contentPane.add(serverAddressField, BorderLayout.NORTH);
        serverAddressField.setColumns(10);

        JPanel centerPanel = new JPanel();
        contentPane.add(centerPanel, BorderLayout.CENTER);

        button1 = new JButton("1");
        button1.setPreferredSize(new Dimension(120, 90));
        button1.setMinimumSize(new Dimension(90, 90));
        centerPanel.add(button1);

        button2 = new JButton("2");
        button2.setPreferredSize(new Dimension(120, 90));
        button2.setMinimumSize(new Dimension(90, 90));
        centerPanel.add(button2);

        button3 = new JButton("3");
        button3.setPreferredSize(new Dimension(120, 90));
        button3.setMinimumSize(new Dimension(90, 90));
        centerPanel.add(button3);

        button4 = new JButton("4");
        button4.setPreferredSize(new Dimension(120, 90));
        button4.setMinimumSize(new Dimension(90, 90));
        centerPanel.add(button4);

        button5 = new JButton("5");
        button5.setPreferredSize(new Dimension(120, 90));
        button5.setMinimumSize(new Dimension(90, 90));
        centerPanel.add(button5);

        button6 = new JButton("6");
        button6.setPreferredSize(new Dimension(120, 90));
        button6.setMinimumSize(new Dimension(90, 90));
        centerPanel.add(button6);

        button7 = new JButton("7");
        button7.setPreferredSize(new Dimension(120, 90));
        button7.setMinimumSize(new Dimension(90, 90));
        centerPanel.add(button7);

        button8 = new JButton("8");
        button8.setPreferredSize(new Dimension(120, 90));
        button8.setMinimumSize(new Dimension(90, 90));
        centerPanel.add(button8);

        button9 = new JButton("9");
        button9.setPreferredSize(new Dimension(120, 90));
        button9.setMinimumSize(new Dimension(90, 90));
        centerPanel.add(button9);

        buttonDot = new JButton(".");
        buttonDot.setPreferredSize(new Dimension(120, 90));
        buttonDot.setMinimumSize(new Dimension(90, 90));
        centerPanel.add(buttonDot);

        button0 = new JButton("0");
        button0.setPreferredSize(new Dimension(120, 90));
        button0.setMinimumSize(new Dimension(90, 90));
        centerPanel.add(button0);

        buttonDel = new JButton("DEL");
        buttonDel.setPreferredSize(new Dimension(120, 90));
        centerPanel.add(buttonDel);

        JPanel southPanel = new JPanel();
        contentPane.add(southPanel, BorderLayout.SOUTH);
        southPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

        exitButton = new JButton("Exit");
        southPanel.add(exitButton);

        connectButton = new JButton("Connect");
        southPanel.add(connectButton);

        connectLButton = new JButton("Connect localhost");
        southPanel.add(connectLButton);

        if ("4.4.50-v7+".equals(System.getProperty("os.version"))) {
            setResizable(false);
            setUndecorated(true); // deletes the frame decoration (the upper bar of the frame)
            gd.setFullScreenWindow(this); // GraphicsEnvironment is essential to create a fullscreen application in unix
        } else {
            setResizable(true);
            setUndecorated(false);
        }
        setVisible(true);

        /**
         *
         * Listeners
         *
         */

        button1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                serverAddressField.setText(serverAddressField.getText() + "1");
            }
        });

        button2.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                serverAddressField.setText(serverAddressField.getText() + "2");
            }
        });

        button3.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                serverAddressField.setText(serverAddressField.getText() + "3");
            }
        });

        button4.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                serverAddressField.setText(serverAddressField.getText() + "4");
            }
        });

        button5.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                serverAddressField.setText(serverAddressField.getText() + "5");
            }
        });

        button6.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                serverAddressField.setText(serverAddressField.getText() + "6");
            }
        });

        button7.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                serverAddressField.setText(serverAddressField.getText() + "7");
            }
        });

        button8.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                serverAddressField.setText(serverAddressField.getText() + "8");
            }
        });

        button9.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                serverAddressField.setText(serverAddressField.getText() + "9");
            }
        });

        button0.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                serverAddressField.setText(serverAddressField.getText() + "0");
            }
        });

        buttonDot.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                serverAddressField.setText(serverAddressField.getText() + ".");
            }
        });

        buttonDel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                serverAddressField.setText(serverAddressField.getText().substring(0, serverAddressField.getText().length() - 1));
            }
        });

        exitButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                System.exit(0);
            }
        });


        /**
         *  Create a communication with the specified server.
         */
        connectButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                String ipAddress = serverAddressField.getText();
                serverAddressField.setText("Trying to connect to a server on " + ipAddress);
                deactivateButtons();
                N.r(() -> {
                    clientService.regAsClient(ipAddress, 8888, 8889)
                            .done(result -> {
                                System.out.println("ClientGui.mouseClicked.Verbunden!");
                                serverAddressField.setText("Connected, waiting for the server to start ...");
                                serverAddressField.setBackground(new Color(0, 255, 0));
                            })
                            .fail(result -> {
                                System.out.println("ClientGui.mouseClicked.Nicht verbinden! Ging was Schief: " + result.toString());
                                activateButtons();
                                serverAddressField.setText("Could not create a connection to the server on " + ipAddress);
                                serverAddressField.setBackground(new Color(255, 0, 0));
                            });
                });
            }
        });

        connectLButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                N.r(() -> {
                    clientService.regAsClient("localhost", 8888, 8889)
                            .done(result -> {
                                System.out.println("ClientGui.mouseClicked.Verbunden!");
                                serverAddressField.setText("Connected, waiting for the server to start ...");
                                serverAddressField.setBackground(new Color(0, 255, 0));
                            })
                            .fail(result -> {
                                System.out.println("ClientGui.mouseClicked.Nicht verbinden! Ging was Schief: " + result.toString());
                                activateButtons();
                                serverAddressField.setText("Could not create a connection to the server on " + "localhost");
                                serverAddressField.setBackground(new Color(255, 0, 0));
                            });
                });
            }
        });
    }

    public String getServerAddressFieldText() {
        return this.serverAddressField.getText();
    }

    public void setServerAddressFieldText(String newText) {
        this.serverAddressField.setText(newText);
    }

    public JTextField getServerAddressField() {
        return this.serverAddressField;
    }

    public void deactivateButtons() {
        button1.setEnabled(false);
        button2.setEnabled(false);
        button3.setEnabled(false);
        button4.setEnabled(false);
        button5.setEnabled(false);
        button6.setEnabled(false);
        button7.setEnabled(false);
        button8.setEnabled(false);
        button9.setEnabled(false);
        button0.setEnabled(false);
        buttonDot.setEnabled(false);
        buttonDel.setEnabled(false);
        exitButton.setEnabled(false);
        connectButton.setEnabled(false);
        connectLButton.setEnabled(false);
    }

    public void activateButtons() {
        button1.setEnabled(true);
        button2.setEnabled(true);
        button3.setEnabled(true);
        button4.setEnabled(true);
        button5.setEnabled(true);
        button6.setEnabled(true);
        button7.setEnabled(true);
        button8.setEnabled(true);
        button9.setEnabled(true);
        button0.setEnabled(true);
        buttonDot.setEnabled(true);
        buttonDel.setEnabled(true);
        exitButton.setEnabled(true);
        connectButton.setEnabled(true);
        connectLButton.setEnabled(true);
    }

    public void setConnectListener(ActionListener l) {
        this.connectButton.addActionListener(l);
    }
}
