package uni.hro.client;

import uni.hro.draw.neuronalnetdrawer.NeuronalNetDrawerJFrame;
import uni.hro.draw.*;
import uni.hro.model.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ConcurrentModificationException;
import java.util.HashSet;

/**
 * @author Joerg, Johann
 *         <p>
 *         Das AquariumJPanel dient als Zeichenfl�che f�r das Aquarium und alle
 *         Lebewesen und Objekte, die sich darin befinden.
 */
public class AquariumJPanel extends JPanel {

	/**
	 * Aquariumwasserfarbe
	 */
	Color backgroundColor = new Color(18, 120, 190);

	/**
	 * Groesse des Fensters
	 */
	int displayWidth;
	int displayHeight;

	/**
	 * Zur Ausgabe der Frames pro Sekunde
	 */
	int frames = 0;
	long firstFrame;
	long currentFrame;
	int fps;
	/**
	 * Speichern Steine und Pflanzen separat.
	 */
	HashSet<Stone> setOfStones = new HashSet<>();
	HashSet<Plant> setOfPlants = new HashSet<>();
	/**
	 * Legt fest, ob der Uhrmodus aktiviert ist
	 */
	boolean showClock = false;

	/**
	 * Zur Anzeige von Datum und Uhrzeit
	 *
	 */
	DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
	LocalDateTime now = LocalDateTime.now();

	/**
	 * Generation, zu der gesprungen werden soll und die aktelle Generation
	 */
	int jumpNumberOfGenerations = 0;
	int currentGeneration = 0;

	int bestFishFitness = 0;
	Fish bestFish = null;
	/**
	 * Popupmenu
	 */
	private JPopupMenu pop;
	private World world;
	private boolean fastforward;
	private JPanel panel;
	private boolean popShown = false;
	private JMenuItem closeWindowItem;
	// System.out.println(dtf.format(now)); //2016/11/16 12:08:43

	/**
	 * Konstruktur des AquariumJPanel
	 *
	 * @param world
	 *            Die Welt, in der die Simulation stattfindet.
	 */
	public AquariumJPanel(World world) {
		this.displayWidth = world.getWidth();
		this.displayHeight = world.getHeight();
		this.world = world;
		this.pop = new JPopupMenu();
		menu();

		panel = this;

		prepareDrawing();
		repaint();

		this.addMouseListener(new MouseAdapter() {
			@Override

			public void mouseClicked(MouseEvent e) {
				// open the popup menu, if left click performed
				if (e.getButton() == MouseEvent.BUTTON1 && e.getClickCount() == 1 && popShown == false) {
					pop.show(panel, e.getX(), e.getY());
					popShown = true;
				} else {
					// close popup menu, if left click is performed on the the
					// JPanel
					pop.setVisible(false);
					popShown = false;
				}
			}
		});
	}

	public HashSet<Plant> getSetOfPlants() {
		return setOfPlants;
	}

	public void setSetOfPlants(HashSet<Plant> setOfPlants) {
		this.setOfPlants = setOfPlants;
	}

	/**
	 * Alle in der Welt vorkommenden Pflanzen und Steine werden separat
	 * gespeichert.
	 */
	public void prepareDrawing() {

		bestFishFitness = 0;
		for (Object s : new HashSet<>(world.getEntities())) {

			if (s instanceof Fish) {

				// System.out.println(((Fish) s).getChromosome());
				if (((Fish) s).getChromosome() != null) {
					int fishFitness = ((Fish) s).getChromosome().getFitnessValue();

					if (fishFitness >= bestFishFitness) {
						bestFishFitness = fishFitness;
						bestFish = ((Fish) s);
					}
					// System.out.println(fishFitness);
				}
			}

			if (s instanceof Stone) {

				Stone newStone = (Stone) s;

				setOfStones.add(newStone);
			}

			if (s instanceof Plant) {

				Plant plant = (Plant) s;
				setOfPlants.add(plant);
			}

		}

		for (Object s : new HashSet<>(world.getEntities())) {

			if (s instanceof Fish) {

				if (((Fish) s).getChromosome() != null) {
					int fishFitness = ((Fish) s).getChromosome().getFitnessValue();

					if (fishFitness == bestFishFitness) {

						Color Goldfarbe = new Color(255, 230, 60);
						// Color Goldfarbe = new Color(254, 254, 254);
						((Fish) s).setColor(Goldfarbe);
						((Fish) s).setCurrentColor(Goldfarbe);

					}
				}
			}
		}
	}

	/**
	 * Bestimmt den Aufbau des Popup-Menus
	 */
	public void menu() {
		JSeparator sep = new JSeparator();
		JMenuItem informationItem = new JMenuItem("Show best Neuronal Net");
		JMenuItem stopJumping = new JMenuItem("Stop Jumping");
		JMenuItem jumpFive = new JMenuItem("Jump 5 Generations");
		JMenuItem jumpOneHundred = new JMenuItem("Jump 25 Generations");
		JMenuItem jumpFiveHundred = new JMenuItem("Jump 100 Generations");
		JMenuItem jumpUnlimited = new JMenuItem("Jump unlimited Generations");
		JSeparator sep2 = new JSeparator();
		JMenuItem showHideClock = new JMenuItem("show / hide Clock ");
		JSeparator sep3 = new JSeparator();
		closeWindowItem = new JMenuItem("End Simulation");

		pop.add(informationItem);
		pop.add(sep);
		pop.add(stopJumping);
		pop.add(jumpFive);
		pop.add(jumpOneHundred);
		pop.add(jumpFiveHundred);
		pop.add(jumpUnlimited);
		pop.add(sep2);
		pop.add(showHideClock);
		pop.add(sep3);
		pop.add(closeWindowItem);

		informationItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {

				// TODO:
				if (bestFishFitness <= 0) {

					Fish lastFish = null;
					for (Object object : new HashSet<>(world.getEntities())) {

						if (object instanceof Fish) {
							lastFish = (Fish) object;
						}

					}
					NeuronalNetDrawerJFrame neuronalNetDrawerJFrame = new NeuronalNetDrawerJFrame();
					neuronalNetDrawerJFrame.drawChromosome( lastFish.getChromosome());
				}

				else {

					NeuronalNetDrawerJFrame neuronalNetDrawerJFrame = new NeuronalNetDrawerJFrame();
					neuronalNetDrawerJFrame.drawChromosome(bestFish.getChromosome());

				}
			}
		});

		showHideClock.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {

				if (showClock == true) {
					showClock = false;
				} else {
					showClock = true;
				}

				pop.setVisible(false);
			}
		});

		stopJumping.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {

				fastforward = false;
				setJumpNumberOfGenerations(currentGeneration - 1);

				pop.setVisible(false);
				popShown = false;

			}
		});

		jumpFive.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {

				fastforward = true;
				setJumpNumberOfGenerations(currentGeneration + 5);
				pop.setVisible(false);
				popShown = false;

			}
		});

		jumpOneHundred.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				fastforward = true;
				setJumpNumberOfGenerations(currentGeneration + 25);
				pop.setVisible(false);
				popShown = false;

			}
		});

		jumpFiveHundred.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				fastforward = true;
				setJumpNumberOfGenerations(currentGeneration + 100);
				pop.setVisible(false);
				popShown = false;

			}
		});

		jumpUnlimited.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				fastforward = true;
				setJumpNumberOfGenerations(999999999);
				pop.setVisible(false);
				popShown = false;

			}
		});

	}

	public void setCloseListener(ActionListener l) {
		this.closeWindowItem.addActionListener(l);
	}

	// @Override

	/**
	 * Gibt an, was auf das AquariumJPanel gezeichnet werden soll.
	 */
	public void paintComponent(Graphics g) {

		super.paintComponent(g);

		setBackground(backgroundColor);

		// nun in paint() / update() bzw. paintComponent() ...
		frames++;
		currentFrame = System.currentTimeMillis();
		if (currentFrame > firstFrame + 1000) {
			firstFrame = currentFrame;
			fps = frames;
			frames = 0;
		}
		String fpss = "FPS: " + fps;
		g.drawString(fpss, 0, 10);

		/**
		 * Anzeige der aktuellen Uhrzeit
		 */
		if (showClock == true) {
			Graphics2D g2 = (Graphics2D) g;
			g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			g.translate(displayWidth - 200, 45);
			g.setFont(new Font("Dialog", Font.BOLD, 40));
			g.setColor(new Color(236, 226, 254));
			LocalDateTime now = LocalDateTime.now();
			g.drawString(dtf.format(now), 0, 10);
			g.translate(-(displayWidth - 200), -45);
		}

		/**
		 * Boden zeichnen
		 */
		GroundDrawer.draw(g, displayWidth, displayHeight, new Color(232, 235, 224), 1, 1);

		try {
			/**
			 * Zeichnen von Kreaturen u
			 */
			for (Object s : new HashSet<>(world.getEntities())) {

				if (s instanceof Fish) {
					Fish fish = (Fish) s;

					if (fish.isAlive()) {

						FishDrawer.draw(g, fish);
						if (showClock == false) {
							HitboxDrawer.draw(g, fish);
						}
					} else {
						DeadFishDrawer.draw(g, fish);
					}

				}

				if (s instanceof Kraken) {
					Kraken kraken = (Kraken) s;

					if (kraken.isAlive()) {
						KrakenDrawer.draw(g, kraken);
						if (showClock == false) {
							HitboxDrawer.draw(g, kraken);
						}
					} else {
						DeadKrakenDrawer.draw(g, kraken);
					}
				}

				if (s instanceof Food) {
					Food food = (Food) s;
					FoodDrawer.draw(g, food);
				}
				/**
				 * if (s instanceof Plant) { Plant plant = (Plant) s;
				 * PlantDrawer.draw(g, plant); }
				 *
				 * if (s instanceof Stone) { Stone stone = (Stone) s;
				 * StoneDrawer.draw(g, stone); }
				 */
			}

			/**
			 * Pflanzen und Steine werden aus separaten Listen abgerufen. Die
			 * Objekte in den Hashsets geraten ducheinander und wuerden
			 * anderenfalls auch immer in unterschiedlicher Reihenfolge
			 * gezeichnet werden => Mal waere ein Stein vorne, dann wieder eine
			 * Pflanze... usw.
			 */

			for (Stone stone : setOfStones) {

				StoneDrawer.draw(g, stone);
			}

			for (Plant plant : setOfPlants) {

				PlantDrawer.draw(g, plant);
			}
		} catch (ConcurrentModificationException e) {
			System.err.println("AquariumJPanel.paintComponent().drawing had concurrency issues. no problem so far.");
		}

		if (fastforward == true) {

			FastForwardSymbolDrawer.draw(g, displayWidth, displayHeight);
			fastforward = false;
		}
	}

	/**
	 * 
	 * Getter und Setter
	 */

	public World getWorld() {
		return world;
	}

	public int getJumpNumberOfGenerations() {
		return jumpNumberOfGenerations;
	}

	public void setJumpNumberOfGenerations(int jumpNumber) {
		this.jumpNumberOfGenerations = jumpNumber;
	}

	public boolean isFastforward() {
		return fastforward;
	}

	public void setFastforward(boolean fastforward) {
		this.fastforward = fastforward;
	}

	public int getCurrentGeneration() {
		return currentGeneration;
	}

	public void setCurrentGeneration(int currentGeneration) {
		this.currentGeneration = currentGeneration;
	}
}
