package uni.hro.networking.service;

import de.mein.auth.jobs.Job;
import de.mein.auth.jobs.ServiceMessageHandlerJob;
import de.mein.auth.service.MeinAuthService;
import de.mein.auth.service.MeinService;
import de.mein.auth.socket.process.val.MeinValidationProcess;
import de.mein.auth.socket.process.val.Request;
import de.mein.auth.tools.N;
import de.mein.sql.SqlQueriesException;
import org.jdeferred.Promise;
import uni.hro.model.Bucket;
import uni.hro.model.Fish;
import uni.hro.model.World;
import uni.hro.simulation.Simulation;

import java.io.File;

/**
 * Created by xor on 5/15/17.
 */
public abstract class AquaExample extends MeinService{ //oder AquaService

    public AquaExample(MeinAuthService meinAuthService, File serviceInstanceWorkingDirectory) {
        super(meinAuthService, serviceInstanceWorkingDirectory);
    }

    /**
     * ist kein live beispiel!
     *
     * @throws InterruptedException
     * @throws SqlQueriesException
     */
    private void how2Communicate() throws InterruptedException, SqlQueriesException {
        // wir wollen einen Eimer Fische verschicken und auf antwort warten!

        Bucket eimer = new Bucket();
        World world = new Simulation(20, 3, 10, 10, 400, 300)
                .getWorld();
        eimer.addFish(new Fish(world, 5, 6), new Fish(world, 50, 60));

        /* id des ziels muss man sich irgendwo sinnvoll merken. ich tue mal so als wüssten wir die.*/
        final long partnerCertificateId = 0L;
        /* die UUID des gegenüber laufenden services muss man sich auch merken. da können theoretisch ganz viele von laufen.*/
        final String partnerServiceUuid = "bla";
        /*
         * hier konnektieren wir. dabei kommt sofort das promise zurück (blockiert hier also nicht).
         * nun kann die verbindung fehlschlagen oder auch erfolgreich zustanden kommen.
         * */
        Promise<MeinValidationProcess, Exception, Void> promise = meinAuthService.connect(partnerCertificateId);
        /* wenn nun irgendwas davon eintritt, wird das ausgeführt, was man dem Promise für den entsprechenden fall vorgibt.*/
        promise.done(validationProcess -> N.r(() -> { // N.r() entspricht einem try/catch-block und macht printStackTrace per default
            /* validationProcess sagt uns, dass die andere seite sicher die ist, für die sie sich ausgibt (validiert, quasi). sicherheitsgedöns.
             * für uns nicht weiter wichtig.
             * damit kann man nun nachrichtenobjekte verschicken oder fragen/requests stellen.
             * Request ist generisches Promise und enthält unsere antwort (in diesem Fall wieder ein Eimer) oder Exception.
             */
            Request<Bucket> request = validationProcess.request(partnerServiceUuid, "MEINE_INTENTION", eimer);
            request.done(andererEimer -> System.out.println("hab einen Eimer zurückbekommen"));
            request.fail(Throwable::printStackTrace);
            /*
             * auf der anderen Seite kommen die requests bei handleRequest() an. die werden bei uns erstmal einem Worker übergeben, so dass wir uns
             * hoffentlich kein großen probleme mit multithreading bekommen.
             */
        }));
        promise.fail(Throwable::printStackTrace);
    }

    /**
     * auch kein live beispiel
     *
     * @param job
     */
    private void workWorkExample(Job job) {
        if (job instanceof ServiceMessageHandlerJob) {
            ServiceMessageHandlerJob messageHandlerJob = (ServiceMessageHandlerJob) job;
            Request request = messageHandlerJob.getRequest();
            Bucket bucket = (Bucket) request.getPayload();
            //irgendwas mit bucket tun und irgendeine Antwort schicken. hier geht ein leerer eimer zurück.
            request.resolve(new Bucket());
            //alternativ einen fehler:
            request.reject(new Exception("alles doof!"));
            // geht aber nur eins davon ;)
        }
    }
}
