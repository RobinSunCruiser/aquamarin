package uni.hro.model;

import java.util.*;

public class HitBox {


    private IntegerPoint offset;
    private Creature creature;
    private Collection<WorldEntity> perceivedEntities = new LinkedList<>();

    public HitBox(Creature creature, IntegerPoint offset) {
        this.creature = creature;
        this.offset = offset;
    }

    public HitBox() {
    }


    public void clear() {
        perceivedEntities.clear();
    }

    public HitBox addPerceivedEntity(WorldEntity entity) {
        perceivedEntities.add(entity);
        return this;
    }


    public final static float HALF_WIDTH = 12;
    public final static float WIDTH = HALF_WIDTH * 2;


    public int getLeftX() {
        float leftX = creature.getX() - HALF_WIDTH;
        float delta = offset.getX() * WIDTH;
        if (creature.looksLeft())
            delta *= -1;
        return (int) (leftX + delta);
    }

    public int getRightX() {
        float leftX = creature.getX() + HALF_WIDTH;
        float delta = offset.getX() * WIDTH;
        if (creature.looksLeft())
            delta *= -1;
        return (int) (leftX + delta);
    }

    public int getTopY() {
        float y = creature.getY() - HALF_WIDTH;
        float delta = offset.getY() * WIDTH;
        return (int) (y + delta);
    }

    public int getBottomY() {
        float y = creature.getY() + HALF_WIDTH;
        float delta = offset.getY() * WIDTH;
        return (int) (y + delta);
    }

    public Collection<WorldEntity> getPerceivedEntities() {
        return perceivedEntities;
    }
}
