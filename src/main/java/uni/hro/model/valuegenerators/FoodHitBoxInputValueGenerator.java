package uni.hro.model.valuegenerators;

import uni.hro.model.Food;
import uni.hro.model.HitBox;
import uni.hro.model.HitBoxInputValueGenerator;

/**
 * Created by xor on 5/29/17.
 */
public class FoodHitBoxInputValueGenerator extends HitBoxInputValueGenerator<Food> {
    public FoodHitBoxInputValueGenerator(HitBox hitBox, Class<Food> clazz) {
        super(hitBox, clazz);
    }
}
