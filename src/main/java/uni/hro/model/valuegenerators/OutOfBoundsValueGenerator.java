package uni.hro.model.valuegenerators;

import uni.hro.model.HitBox;
import uni.hro.model.HitBoxInputValueGenerator;
import uni.hro.model.OutOfBounds;

/**
 * Created by xor on 6/18/17.
 */
public class OutOfBoundsValueGenerator extends HitBoxInputValueGenerator<OutOfBounds> {
    public OutOfBoundsValueGenerator(HitBox hitBox, Class<OutOfBounds> clazz) {
        super(hitBox, clazz);
    }
}
