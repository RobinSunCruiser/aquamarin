package uni.hro.model.valuegenerators;

/**
 * Created by xor on 7/4/17.
 */
public interface InputValueGenerator {
    double getValue();
}
