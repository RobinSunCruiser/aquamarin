package uni.hro.model.valuegenerators;

import uni.hro.model.Fish;
import uni.hro.model.HitBox;
import uni.hro.model.HitBoxInputValueGenerator;

/**
 * Created by xor on 5/23/17.
 */
public class FishHitBoxInputValueGenerator extends HitBoxInputValueGenerator<Fish> {

    public FishHitBoxInputValueGenerator(HitBox hitBox, Class<Fish> clazz) {
        super(hitBox, clazz);
    }
}
