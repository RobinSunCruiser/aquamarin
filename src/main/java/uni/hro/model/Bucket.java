package uni.hro.model;

import de.mein.auth.data.IPayload;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by xor on 5/13/17.
 */
public class Bucket implements IPayload{
    private List<Fish> fishes = new ArrayList<>();

    public List<Fish> getFishes() {
        return fishes;
    }

    public Bucket addFish(Fish... fishes){
        this.fishes.addAll(Arrays.asList(fishes));
        return this;
    }
}
