/**
 * 
 */
package uni.hro.model;

import uni.hro.draw.StoneDrawer;

/**
 * @author Columbus
 *
 */
public class Stone extends WorldEntity {

	protected World world;

	protected int[] xGeometriePoints;
	protected int[] yGeometriePoints;
	
	/**
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 */
	public Stone(World world, float x, float y, float width, float height) {
		super(x, y, width, height);
		this.world = world;

		StoneDrawer stonedrawer = new StoneDrawer();
		this.xGeometriePoints = stonedrawer.createRandomizedGeometrieValuesX();
		this.yGeometriePoints = stonedrawer.createRandomizedGeometrieValuesY();
		stonedrawer=null;
	
	}

	public int[] getxGeometriePoints() {
		return xGeometriePoints;
	}

	public void setxGeometriePoints(int[] xGeometriePoints) {
		this.xGeometriePoints = xGeometriePoints;
	}

	public int[] getyGeometriePoints() {
		return yGeometriePoints;
	}

	public void setyGeometriePoints(int[] yGeometriePoints) {
		this.yGeometriePoints = yGeometriePoints;
	}


}
