package uni.hro.model;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by xor on 4/24/17.
 */
public class CreatureTest {
    private static int DEFAULT_POSITION = 20;
    private static int DEFAULT_WIDTH = 20;
    private static World world = new World(1000, 1000);

    public static class Square extends Creature {
        public Square(World world, int x, int y) {
            super(world, 100, x, y, DEFAULT_WIDTH, DEFAULT_WIDTH);
            createNeuron(x,y);
        }

        public Square(World world, int x, int y, int width) {
            super(world, 100, x, y, DEFAULT_WIDTH, DEFAULT_WIDTH);
            createNeuron(x,y);
        }

        @Override
        public void handleCollisions() {

        }
    }

    private static Square s1, s2;

    @Test
    public void topRight() {
        setup(DEFAULT_POSITION + 5, DEFAULT_POSITION - 5);
        s1.perceive();
        assertTrue(s1PerceivedS2());
    }

    @Test
    public void topLeft() {
        setup(DEFAULT_POSITION - 5, DEFAULT_POSITION - 5);
        s1.perceive();
        assertTrue(s1PerceivedS2());
    }

    @Test
    public void bottomLeft() {
        setup(DEFAULT_POSITION - 5, DEFAULT_POSITION + 5);
        s1.perceive();
        assertTrue(s1PerceivedS2());
    }

    @Test
    public void bottomRight() {
        setup(DEFAULT_POSITION + 5, DEFAULT_POSITION + 5);
        s1.perceive();
        assertTrue(s1PerceivedS2());
    }

    @Test
    public void noCut() {
        setup(50, 50);
        s1.perceive();
        assertFalse(s1PerceivedS2());
    }

    @Test
    public void enclosingBigger() {
        setupEnclosing(30);
        s1.perceive();
        assertTrue(s1PerceivedS2());
    }

    @Test
    public void enclosingSmaller() {
        setupEnclosing(5);
        s1.perceive();
        assertTrue(s1PerceivedS2());
    }

    private boolean s1PerceivedS2() {
        return s1.neurons.keySet().iterator().next().getPerceivedEntities().size() > 0;
    }

    private void setup(int x2, int y2) {
        s1 = new Square(world, DEFAULT_POSITION, DEFAULT_POSITION);
        s2 = new Square(world, x2, y2);
        world.getEntities().clear();
        world.getEntities().add(s1);
        world.getEntities().add(s2);
    }

    private void setupEnclosing(int width2) {
        s1 = new Square(world, DEFAULT_POSITION, DEFAULT_POSITION);
        s2 = new Square(world, DEFAULT_POSITION, DEFAULT_POSITION, width2);
        world.getEntities().clear();
        world.getEntities().add(s1);
        world.getEntities().add(s2);
    }
}
