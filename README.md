# This complex software project was build in SS 2017 at the university of rostock
Student developers: Eric Büchner, Richard Dabels, Daniel Decker, Tom Ettrich, Johann Kluth, Jörg Stüwe

Supervisor: Robin Nicolay

# Important Links
* [Raspberry Image mit Aquarium vom 30.6.2017](http://www.informatik.uni-rostock.de/~robin/KSWSRPI.img.zip)

# Projekt clonen, bauen und starten

## Setup

### Eclipse installieren
* Download und install [Eclipse](https://www.eclipse.org/downloads/?) für Java Developers
* Eclipse starten und *workspace* auswählen
* Über *Hilfe -> Check* for Updates Eclipse aktualisieren
 
### Projekt in Eclipse clonen
* *Welcome* Bildschirm schließen und *Git-Ansicht* öffnen
* *https://robin.nicolay@git.informatik.uni-rostock.de/robin.nicolay/aquamarin.git* kopieren und mit Rechtsklick auf *Git-Ansicht* einfügen
* *Master-Branch* auschecken

### Projekt mit Gradle bauen
* Zurück zur *Java-Ansicht* wechseln und über *Import -> Gradle -> Gradle Project* auswählen und in *Projekt root directory* das ausgecheckte *aquamarin*-Verzeichnis auswählen
* Finish

## Starten
* In *uni.hro.main.AquaServer.java* auswählen und *Run* drücken
* *Java Application* wählen (Aktuell gibt es hier einige Fehler, die noch beseitigt werden)
* Falls die Simulation nicht los läuft das gespeicherte Netz durch Auswahl von *reset* im Startscreen zurücksetzen

# Aufgabenstellung
Inspiration für das Projekt: [Guppies](https://www.youtube.com/watch?v=tCPzYM7B338)

Für Touchdisplays von Raspberries soll eine Aquariumssimulation entwickelt werden. Es wird ein Habitat entwickelt in dem es Nahrung und Feinde für Fische gibt. Die Fische selbst werden durch Agenten gesteuert und haben Sensoren mit denen sie ihre Umgebung wahrnehmen. Mit hilfe evolutionärer Algorithmen sollen die Fische selbstständig lernen in diesem Habitat zu überleben. Falls noch Zeit ist, sollen mehrere Bildschirme zu einem größeren Aquarium zusammengeschlossen werden können.

## Mit Git das Projekt clonen und Zugriff einrichten

Um berechtigt zu sein ins Git zu pushen, muss der eigene Git-Account zum Projekt eingeladen sein und der Rechner über einen gültigen SSH-Key mit dem eigenen Git-Account verbunden sein.

#### SSH KEY erstellen
Im Terminal des (Linux-)Rechners ```ssh-keygen``` ausführen. 
Dann den öffentlichen Teil des Schlüssels mit ```cat ~/.ssh/id_rsa.pub``` ausgeben.
Die Zeilen *"ssh-rsa ....usw."* kopieren und auf der  GitLab-Seite als SSH-Key deinem Benutzeraccount hinzufügen.

#### Accountverwaltung SSH-Keys
Navigiere auf der Gitlab-Seite zu:  *Benutzer -> Settings -> SSH Keys* und füge den kopierten Schlüsseltext ein.

Der Rechner kann jetzt unter deinem Namen in dir zugeordnete Projekte *pushen*.

#### Git Projekt Clonen
Das Projekt aus dem Git herunterladen (im homeverzeichnis)

```cd ~```

```git clone git@git.informatik.uni-rostock.de:robin.nicolay/aquamarin.git```

#### Änderungen pushen
Um Änderungen in das Git-Verzeichnis hochzuladen müssen informationen gepushed werden
Hierzu alle geänderten Dateien hinzufügen, einen Commit erstellen und dann pushen.
Hier am Beispiel des Default-Branches "master"

```git add -A```

```git commit -m "Hier die Notiz zum Commit eingeben"```

```git push -u origin master```

## RasPi HDMI Bildschirm und Setup

## HDMI und Waveshare LCD Bildschirm
[Eine Wordpress-Dokumentation](http://lion.i234.me/wordpress/2017/03/02/3-5-waveshare-lcd-und-raspbian/)

LCD -> HDMI: ```./LCD-hdmi```

HDMI -> LCD: ```./LCD35-show```

LCD Auflösung: 480x320

### Aquarium auf Raspberry
1. Win32DiskImager installieren
2. SD-Card aus dem Raspi nehmen und in einen SD-Kartenleser packen
3. Laufwerk und die beiliegende Image-Datei im DiskImager auswählen
4. Schreiben beginnen
...
5. SD-Card wieder in den Raspi packen
6. Raspi starten
	NoMachine ist auf dem Image bereits installiert. Ihr könnt euch auf eurem Endgerät auch eine NoMachine Installation
	anlegen und die Raspis damit recht bequem steuern (einfach über die IP-Adresse im lokalen Netzwerk).
	NoMachine ist wie TeamViewer und läfut auch gut auf dem Raspi.
7. Ein Terminal aufmachen und via "cd ksws" in den Ordner des Aquariums wechseln
8. **Starte das Aquarium über "java -jar server.jar"**
9. Einstellungen anpassen (ACHTUNG! Die Rechenleistung ist sehr gering!) und starten (mehr Informationen im README des Projekts)

## Mit Eclipse eine Runnable -jar erstellen
1. Projekt in Eclipse Importieren und lauffähig machen
2. Den AquaServer unter "main.java.uni.hro.main" einmal starten
3. Rechtsklick auf das Projekt: Export...
4. "Java/Runnable JAR file" auswählen und auf Next
5. Bei der Launch configuration der AquaServer auswählen und die Ort für die JAR unter Export destination bestimmen
	Library handling: Extract required libraries into generated JAR
6. Finish
7. Wenn die JAR erfolgreich erstellt wurde, kann sie über PSCP (von PuTTY) oder NoMachine einfach auf den Raspi kopiert werden
	(es gibt natürlich noch andere Möglichkeiten)
8. Die erstellte JAR auf dem Raspi am besten wieder in den ksws Ordner packen, oder alternativ einen eigenen Ordner wählen
9. **Starte das Aquarium über "java -jar server.jar"**